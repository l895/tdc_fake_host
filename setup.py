from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="tdc_fake_host",
    version="1.0.0",
    author="Julian Rodriguez",
    author_email="jnrodriguezz@hotmail.com",
    description="This fake host can be used to command the TDC developed by LabOSat through TCP/IP sockets or through UART.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/l895/tdc_fake_host",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: UNIX Based",
    ],
    package_dir = {'' : 'src'},
    packages = find_packages(where='src'),
    install_requires=   [   'pytest>=6.2.5',
                            'pytest-mock==3.6.1',
                            'pandas>=1.3.4',
                            'pyyaml>=5.3.1',
                            'psycopg2-binary==2.9.1',
                            'sqlalchemy',
                            'dash==2.0.0',
                            'dash-core-components==2.0.0',
                            'dash-html-components==2.0.0',
                            'dash-table==5.0.0',
                            'dash-bootstrap-components==1.0.0'
                        ],
    python_requires=">=3.6",
)
