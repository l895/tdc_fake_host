import plotly.graph_objects as go

heimdall_signals_layout_template = dict(
    layout = go.Layout(
        paper_bgcolor = '#31302F',
        plot_bgcolor = '#31302F',
        yaxis = {
            'tick0': 0,
            'dtick': 1
        },

        margin = {
            'l': 20,
            'r': 5,
            'b': 5,
            't': 10
        }
    )
)
