import dash
from dash import dcc, html, dash_table
import plotly.graph_objects as go
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
from plotly.subplots import make_subplots

import numpy as np
import pandas as pd
from datetime import datetime, timezone

from dashboard_agent import HeimdallDashboardAgent
from plots_templates import heimdall_signals_layout_template

# Initialize the app
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

# Create the waveforms_df that will store the signals to plot
# This dataframe at the beginning only have a demo waveform
waveforms_df = pd.read_csv('data/fake_waveforms.csv', index_col=0, parse_dates=True)
waveforms_df.index = pd.to_datetime(waveforms_df['start_ts'])

# The layout defines how looks the web app.
app.layout = html.Div(
    children=[
        html.Div(   className='row',
                    children=[
                        ## In this div we will position the control panel!
                        html.Div(   className='four columns div-user-controls',
                                    children=[
                                        html.Div(
                                            children=[
                                                html.H1('TDC control panel', style={'textAlign':'center', 'margin-bottom':'20 px'}),
                                                html.Div([
                                                    html.H2('Click to measure with the TDC'),
                                                    html.Button(
                                                        'Measure', 
                                                        id='trigger-measure-button', 
                                                        n_clicks=0,
                                                        className='button-big-primary'),
                                                        html.P(id='trigger-measure-message') ## here will be printed the callback msg
                                                    ])
                                                ],
                                            style={'textAlign':'center', 'margin':'auto', 'margin-bottom':'50px'}
                                            ),
                                        # html.Div(
                                        #     children=[
                                        #         html.H2('TDC waveform picker'),
                                        #         html.P('Introduce the Pick one or more measurements from the dropdown below.'),
                                        #         dcc.DatePickerSingle(
                                        #             id='waveform_date_picker',
                                        #             min_date_allowed=datetime(2020, 8, 5),
                                        #             date=datetime.now(timezone.utc)
                                        #             ),
                                        #         html.Div(id='waveform_date_picker_msg_output')
                                        #         ]
                                        #     ),
                                        html.Div(
                                            children=[
                                                html.H2('TDC Measurements made'),                                 
                                                html.P('Pick a measure from the displayed'),
                                                html.Div(
                                                    className='div-for-dropdown',
                                                    children=[
                                                        dash_table.DataTable(
                                                            id='measurements_table',
                                                            style_table={
                                                                'overflowY': 'scroll',
                                                                'overflowX': 'scroll',
                                                                'width': '100%',
                                                                },
                                                            page_size=10,
                                                            row_selectable="single",
                                                            columns=[   {"name": "id", "id": "id", "selectable": True},
                                                                        {"name": "start_time", "id": "start_time", "selectable": True},
                                                                        {"name": "channels", "id": "channels", "selectable": True},
                                                                        {"name": "pulses_captured", "id": "pulses_captured", "selectable": True}
                                                                ],
                                                            style_cell={'textAlign': 'left'},
                                                            style_header={  'backgroundColor': '#135569',
                                                                            'font-family':'monospace',
                                                                            'color':'#E1E1E1',
                                                                            'border':'2px solid #5CCBEC'
                                                                },
                                                            style_data={  'backgroundColor': 'lightgrey solid',
                                                                            'font-family':'monospace',
                                                                            'color':'black',
                                                                            'border':'2px solid #5CCBEC'
                                                                },
                                                        ),
                                                        dcc.Interval(
                                                            id='update_measurements_table_interval',
                                                            interval=30000, # in milliseconds
                                                            n_intervals=0
                                                        ),
                                                        #dbc.Alert(id='measurements_table_out')
                                                    ],
                                                    style={'color': '#1E1E1E'})
                                                ]
                                            )
                                        ]
                            ),
                        ## In this div are the plots!
                        html.Div(   className='eight columns div-for-charts bg-grey',
                                    children=[
                                        dcc.Graph(
                                            id='waveforms_graph',
                                            style={'width': '100vh', 'height': '100vh'}
                                            ),
                                        ]
                            )
                        ]
            )
        ]
)


@app.callback(
    Output('trigger-measure-message', 'children'),
    Input('trigger-measure-button', 'n_clicks')
)
def trigger_measure(btn):
    '''
        When a measure is triggered with the button, this message will appear
        below the button. It's like a check saying 'the measure was triggered, wait 
        for the data!'.
    '''
    return html.P(
        'Measure triggered at {} (UTC)... wait until it appears '
        'in the table'.format(
        datetime.now(timezone.utc).strftime("%Y/%m/%d-%H:%M:%S")))


@app.callback(
    Output('measurements_table', 'data'),
    Input('update_measurements_table_interval', 'n_intervals')
)
def update_measurements_table(n_intervals):
    '''
        Update periodically the table of measurements
        with newer measurements.
    '''
    app.logger.info('updating measurements table ({})...'.format(n_intervals))
    return agent.get_measurements().to_dict('records')


@app.callback(
    Output('waveforms_graph', 'figure'),
    Input('measurements_table', 'selected_row_ids')
)
def update_graphs(selected_row_ids):
    '''
        Update the plots if a measurement was picked in the
        data table. The selected_row_id is the measurement id! 
        so we can fetch this signals waveforms from the 
        database with that value.
    '''
    fig = go.Figure(data=[])
    if selected_row_ids is None:
        app.logger.info('nothing was selected')
    else:
        measurement_id = selected_row_ids[0]
        app.logger.info('measurement {} was picked!'.format(measurement_id))
        waveforms_df = agent.get_waveforms(measurement_id)
        if waveforms_df.empty:
            app.logger.error(
                'measurement {} could not be found or error in backend!'.format(
                    measurement_id
                    )
                )
        else:
            app.logger.info('updating plots!')
            channels_found = np.sort(waveforms_df['channel'].unique())
            fig = make_subplots(
                    rows=len(channels_found), 
                    cols=1,
                    x_title='time (ps)',
                    y_title='logic value'
                )
            current_row = 1
            for channel in channels_found:
                channel_df = waveforms_df[waveforms_df['channel'] == channel].sort_values(by='time_ps')
                fig.add_trace(
                    go.Scatter(
                            y=channel_df['signal_value'],
                            x=channel_df['time_ps'],
                            name=str(channel)
                        ),
                        row=current_row,
                        col=1,
                    )
                current_row += 1
            fig['layout'].update(
                    template=heimdall_signals_layout_template
                )

    return fig


def run_server(db_username, db_password, db_ip, db_port, db_name):
    """
        Run the web app server.
        @param username the username to login to the database that holds the waveforms.
        @param password the password to login to the database that holds the waveforms.
        @param ip the IP of the server that stores the database that holds the waveforms.
        @param port hte port of the server that stores the database that holds the waveforms.
        @param db_name the name of the database where the tables are stored that holds the waveforms.
    """
    # Create the Heimdall Dashboard Agent that brings the data of the waveforms
    global agent
    agent = HeimdallDashboardAgent(
        db_username=db_username,
        db_password=db_password,
        db_ip=db_ip,
        db_port=db_port,
        db_name=db_name)

    """
        Use the host parameter to bind the webserver to all the IPs!
        If not, we cannot acced from the host machine if we run it with docker.
        https://devops.stackexchange.com/questions/3380/dockerized-flask-connection-reset-by-peer

    """
    app.run_server(debug=True, host="0.0.0.0")    

