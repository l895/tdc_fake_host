import pandas as pd

from heimdall_common.db.db_conn import HeimdallDB
from heimdall_common.db.db_tables import WaveformEntry, MeasurementEntry


class HeimdallDashboardAgent:
    def __init__(self, db_username, db_password, db_ip, db_port, db_name):
        """
            Creates a Heimdall database object.
            @param username the username to login to the database.
            @param password the password to login to the database.
            @param ip the IP of the server that stores the database.
            @param port hte port of the server that stores the database.
            @param db_name the name of the database where the tables are stored.
        """
        self.db_params = {
            "ip": db_ip,
            "port": db_port,
            "username": db_username,
            "password": db_password,
            "name": db_name
        }

    def get_measurements(self):
        """
            Get the measurements cycle from the database and store them
            in a dataframe. This are not the waveforms, are only the 
            available measurements stored in the database.
            @return a dataframe with this columns:
                - start_ts: the start time of the measurement
                - channels: a list with the channels measured in that measurement
                - pulses_captured: the quantity of pulses captured in each channel
        """
        db = HeimdallDB(
            username=self.db_params['username'],
            password=self.db_params['password'],
            ip=self.db_params['ip'],
            port=self.db_params['port'],
            db_name=self.db_params['name']
        )
        measurements = db.session.query(MeasurementEntry).all()
        df = pd.DataFrame(columns=['id','start_time', 'channels', 'pulses_captured'])
        for meas in measurements:
            d_to_append = meas.serialize()
            d_to_append['start_time'] = d_to_append['start_time'].strftime("%Y/%m/%d-%H:%M:%S.%f")
            df = df.append(d_to_append, ignore_index=True)
        df.sort_values(by='start_time', ascending=False, inplace=True)
        return df

    def get_waveforms(self, measurement_id):
        """
            Search a waveform by it's measurement id.
            @param measurement_id the ID of the measurement that we are searching.
            @return a dataframe with the samples of the waveforms in that measure or
            returns an empty dataframe if the search fails.
        """
        db = HeimdallDB(
            username=self.db_params['username'],
            password=self.db_params['password'],
            ip=self.db_params['ip'],
            port=self.db_params['port'],
            db_name=self.db_params['name']
        )
        waveforms = db.session.query(WaveformEntry).filter(
            WaveformEntry.measurement_id == measurement_id
        )

        df = pd.DataFrame(columns=['start_time', 'channel', 'time_ps', 'signal_value'])
        for sample in waveforms:
            d_to_append = sample.serialize()
            d_to_append['start_time'] = d_to_append['start_time'].strftime("%Y/%m/%d-%H:%M:%S.%f")
            df = df.append(d_to_append, ignore_index=True)
        df.sort_values(by='start_time', ascending=True, inplace=True)
        return df
