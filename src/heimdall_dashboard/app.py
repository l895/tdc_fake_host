from web import run_server
import argparse

def parse_arguments():
    parser = argparse.ArgumentParser(description='Run the web app that host a dashboard with the waveforms of the TDC.')
    parser.add_argument('-id', '--ip-db',type=str, help='IP of the database server where the data is stored')
    parser.add_argument('-pd', '--port-db', type=str, help='port of the database server where the data is stored')
    parser.add_argument('-ud', '--username-db',type=str, help='Username to login to the database server where the data is stored')
    parser.add_argument('-psd', '--password-db', type=str, help='Password to login to the database server where the data is stored')
    parser.add_argument('-nd', '--name-db', type=str, help='Name of the database where the data is stored.')

    return parser.parse_args()


def cli():
    args = parse_arguments()
    
    run_server(
        db_username=args.username_db,
        db_password=args.password_db,
        db_ip=args.ip_db,
        db_port=args.port_db,
        db_name=args.name_db
    )

if __name__ == '__main__':
    """
        Use the host parameter to bind the webserver to all the IPs!
        If not, we cannot acced from the host machine if we run it with docker.
        https://devops.stackexchange.com/questions/3380/dockerized-flask-connection-reset-by-peer
    """
    cli()