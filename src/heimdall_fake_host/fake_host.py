"""
 *  Fake TDC CLI. With this CLI we can mount an OSOTB server that mocks a
 *  TDC.
 *  Example: python3 -m heimdall_fake_tdc.fake_tdc -i 10.0.0.65 -p 8919 -c 16    
 *  
 *  Created on: Nov 4, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""
import time
import logging
import argparse
from datetime import datetime
import pandas as pd

from heimdall_common.osotb.client.tcpip import OSOTB_TCPClient
from heimdall_common.db.db_conn import HeimdallDB
from heimdall_common.db.db_tables import WaveformEntry, MeasurementEntry

logger = logging.getLogger(__name__)

class HeimdallFakeHost:
    def __init__(   self, ip = "10.0.0.65", port = 8919, 
                    ip_db = "10.0.0.67", port_db = 8941,
                    username_db = "postgres", password_db = "pass", name_db = "heimdall"):
        """
            Init the fake host for the TDC.
            This fake host can communicate with the TDC making OSOTB requests. 
            @param ip the IP of the OSOTB server mounted by the TDC.
            @param port the port of the OSOTB server mounted by te TDC.
            @param ip_db the IP of the database server where the data is stored.
            @param port_df the port of the database server where the data is stored.
            @param username_db the username to login to the database.
            @param password_db the password to login to the database.
            @param name_db the name of the database.
        """
        self.tdc_server_params = { "ip": ip, "port": port }
        self.db_params = {
            "ip": ip_db,
            "port": port_db,
            "username": username_db,
            "password": password_db,
            "name": name_db
        }
        self.current_measurement_id = 0

    def _request_uptime(self):
        """
            Request the uptime to the TDC.
            Send a GET to the resource "uptime".
            @return the uptime of the TDC.
        """
        osotb_client = OSOTB_TCPClient(ip = self.tdc_server_params['ip'], port = self.tdc_server_params['port'])
        ret = None
        osotb_packet = osotb_client.create_osotb_req_packet(resource='uptime', method='GET')
        response = osotb_client.send_request(osotb_packet)
        if response:
            ret = response['payload']
        return ret

    def _request_fifo_data(self):
        """
            Request the FIFO buffer data to the TDC.
            Send a GET to the resource "fifo_data".
        """
        osotb_client = OSOTB_TCPClient(ip = self.tdc_server_params['ip'], port = self.tdc_server_params['port'])
        ret = None
        osotb_packet = osotb_client.create_osotb_req_packet(resource='fifo_data', method='GET')
        response = osotb_client.send_request(osotb_packet)
        if response:
            ret = response['payload']
        return ret

    def _request_op_mode(self):
        """
            Request the actual operation mode of the TDC.
            Send a GET to the resource "op_mode".
            @return the actual operation mode of the TDC.
        """
        osotb_client = OSOTB_TCPClient(ip = self.tdc_server_params['ip'], port = self.tdc_server_params['port'])
        ret = None
        osotb_packet = osotb_client.create_osotb_req_packet(resource='op_mode', method='GET')
        response = osotb_client.send_request(osotb_packet)
        if response:
            ret = response['payload']
        return ret

    def _start_measurement(self):
        """
            Start a measurement cycle.
            Send a PUT to the resource "op_mode" to set it in the 'measure' state.
        """
        osotb_client = OSOTB_TCPClient(ip = self.tdc_server_params['ip'], port = self.tdc_server_params['port'])
        osotb_packet = osotb_client.create_osotb_req_packet(resource='op_mode', method='PUT', payload='measure')
        response = osotb_client.send_request(osotb_packet)
        if response:
            ret = response['header']['status']
        return ret

    def _store_measurement_description_in_db(self, measurement_description):
        """
            Register the measurement in the database.
            @param signals_df a dataframe with the waveform of the signal.
            @param start_ts the timestamp of the start of the measure
        """
        db = HeimdallDB(
            ip = self.db_params['ip'],
            port = self.db_params['port'],
            username = self.db_params['username'],
            password = self.db_params['password'],
            db_name = self.db_params['name']
            )

        logger.info('storing measurement description in database {}...'.format(measurement_description))
        db.add_entry(
            MeasurementEntry(
                start_time = measurement_description['start_time'], 
                channels = measurement_description['channels'],
                pulses_captured = measurement_description['pulses_captured'],
            )
        )

    def _store_waveform_in_db(self, signals_df, start_ts):
        """
            Store the signals waveform in the database.
            @param signals_df a dataframe with the waveform of the signal.
            @param start_ts the timestamp of the start of the measure,
            must be a datetime object.
        """
        logger.info('storing signal waveform in database...')
        db = HeimdallDB(
            ip = self.db_params['ip'],
            port = self.db_params['port'],
            username = self.db_params['username'],
            password = self.db_params['password'],
            db_name = self.db_params['name']
            )

        for i in signals_df.index:
            entry = WaveformEntry(
                measurement_id = self.current_measurement_id,
                start_time = start_ts,
                channel = signals_df['channel'][i],
                time_ps = signals_df['time_ps'][i],
                signal_value = signals_df['value'][i]
            )
            db.add_entry(entry)

    def _build_measurement_description_from_data(self, fifo_data_df, start_ts):
        """
            Build a register that describes the measurement cycle.
            @param fifo_data_df the dataframe that stores the data registers.
            This dataframe must have te following columns:
                - "channel" = the channel from where the signal was measured. 
                - "pulse_number" = the pulse number of measure cycle in a determined channel
                - "time_width_ps" = the time width reported by the register
                - "register_type" = 'pulse_width' or 'pulse_distance_width'
            @param start_ts the timestamp of the start of the measure,
            must be a datetime object.
            @return a dictionary that describes the measurement cycle.
            The dictionary have 3 columns:
                - "start_time" = the datetime in which the measurement started.
                - "channels" = the channels measured .
                - "pulses_captured" = the quantity of pulses measured in that cycle.
        """
        pulses_captured = 0
        for ch in fifo_data_df['channel'].unique():
            pulses_captured += fifo_data_df[ fifo_data_df['channel'] == ch ]['pulse_number'].astype(int).max()+1 # plus 1 is becasue we count from 0 the pulses

        measurement_decription = {
            'start_time': start_ts,
            'channels': str(set(fifo_data_df['channel'].astype(int).unique())),
            'pulses_captured': pulses_captured.item(),
            }

        return measurement_decription

    def _build_signals_from_data(self, fifo_data_df):
        """
            Build the digital signals from the data registers and store them in a
            dataframe.
            @param fifo_data_df the dataframe that stores the data registers.
            This dataframe must have te following columns:
                - "channel" = the channel from where the signal was measured. 
                - "pulse_number" = the pulse number of measure cycle in a determined channel
                - "time_width_ps" = the time width reported by the register
                - "register_type" = 'pulse_width' or 'pulse_distance_width'
            @return a dataframe that have the values of the signal over time.
            The dataframe have 3 columns:
                - "channel" = the channel from where the signal was measured. 
                - "time_ps" = the time in picoseconds from the beginning of the measure
                - "value" = 0 or 1, the logical value of the signal 
        """
        signals_df = pd.DataFrame(columns = [
                "channel", "time_ps", "value"
        ])
        for channel in fifo_data_df['channel'].unique():
            logger.info('building waveform for channel {}'.format(channel))
            channel_df = fifo_data_df[ fifo_data_df['channel'] == channel ]
            pulse_width_df = channel_df[ channel_df['register_type'] == 'pulse_width' ].sort_values(by='pulse_number', axis=0, ascending=True)
            pulse_distance_width_df = channel_df[ channel_df['register_type'] == 'pulse_distance_width' ].sort_values(by='pulse_number', axis=0, ascending=True)

            accummulated_time_ps = 0
            if not pulse_width_df[ pulse_width_df['pulse_number'] == 0 ]['time_width_ps'].empty:
                signals_df = signals_df.append(
                        {'channel' : channel, 'time_ps' : 0, 'value' : 1},
                        ignore_index = True
                )
                accummulated_time_ps += pulse_width_df[ pulse_width_df['pulse_number'] == 0 ]['time_width_ps'].iloc[0]
                signals_df = signals_df.append(
                        {'channel' : channel, 'time_ps' : accummulated_time_ps, 'value' : 1},
                        ignore_index = True
                    )
            else:
                signals_df = signals_df.append(
                        {'channel' : channel, 'time_ps' : 0, 'value' : 0},
                        ignore_index = True
                )
                logger.debug('no information about pulse 0!')
            signals_df = signals_df.append(
                {'channel' : channel, 'time_ps' : accummulated_time_ps+1, 'value' : 0}, # register when the pulse ends
                ignore_index = True
            )

            for i in pulse_distance_width_df.index:
                pulse_number = pulse_distance_width_df.loc[i,'pulse_number']
                logger.debug('reconstructing pulse %d for channel %d', pulse_number, channel)
                distance_from_previous_pulse = pulse_distance_width_df.loc[i, 'time_width_ps']
                accummulated_time_ps += distance_from_previous_pulse

                if not pulse_width_df[ pulse_width_df['pulse_number'] == pulse_number ].empty:
                    pulse_width = pulse_width_df[ pulse_width_df['pulse_number'] == pulse_number ]['time_width_ps'].iloc[0]
                else:
                    logger.warning('no width information for pulse %d, reporting 1ps', pulse_number)
                    pulse_width = 1

                signals_df = signals_df.append(
                        [
                            {'channel' : channel, 'time_ps' : accummulated_time_ps-1, 'value' : 0},
                            {'channel' : channel, 'time_ps' : accummulated_time_ps, 'value' : 1}, # register when the pulse starts
                            {'channel' : channel, 'time_ps' : accummulated_time_ps+pulse_width, 'value' : 1}, # register how long the pulse is
                            {'channel' : channel, 'time_ps' : accummulated_time_ps+pulse_width+1, 'value' : 0} # register when the pulse ends
                        ],
                        ignore_index = True
                    )
                accummulated_time_ps += pulse_width

        return signals_df

    def _process_fifo_data_register(self, fifo_data_register):
        """
            Process the information of a FIFO data register and returns it
            in a dictionary fashion.
            @param fifo_data_register the 32 bit data register to process. Must to
            be an integer number, its the value of the register.
            @return a dictionary with the data of the register.
        """
        processed_register = dict()
        
        processed_register['channel'] = (fifo_data_register >> 27) & 0x1F # bits [31-27]
        processed_register['pulse_number'] = (fifo_data_register >> 20) & 0x7F # bits [26-20]
        processed_register['time_width_ps'] = (fifo_data_register >> 0) & 0x7FFFF # bits [18-0]
        processed_register['register_type'] = 'pulse_width' if ((fifo_data_register & (1 << 19)) != 0) else 'pulse_distance_width'
        
        return processed_register

    def get_measurements_made(self):
        """
            Get the measurements made searching it in the database.
            @return the quantity of measurements made
        """
        db = HeimdallDB(
            ip = self.db_params['ip'],
            port = self.db_params['port'],
            username = self.db_params['username'],
            password = self.db_params['password'],
            db_name = self.db_params['name']
            )

        return db.session.query(MeasurementEntry).count()

    def measure(self):
        """
            Perform a measure cycle with the TDC, 
            parse the data and store it in a database.
            @param measure_cycles_qty the quantity of measure cycles
            that the fake host will do. 
        """
        self.current_measurement_id = self.get_measurements_made()

        start_ts = datetime.utcnow()
        logger.info('getting uptime to check aliveness')
        uptime = self._request_uptime()
        if uptime:
            logger.info('uptime of the TDC is: {}, starting measurement...'.format(uptime))
            time.sleep(5)
            if self._start_measurement() == '200 Ok':
                logger.info('waiting until the measurement finish...')
                while self._request_op_mode() != 'idle':
                    time.sleep(1)

                logger.info('measurement finished... reading FIFO buffer data...')
                
                fifo_data_df = pd.DataFrame(columns = [
                    "channel", "pulse_number", "time_ps", "register_type"
                ])

                fifo_data_register = self._request_fifo_data()
                while fifo_data_register != '0xFFFFFFFF':
                    if not fifo_data_register:
                        logger.error('error reading FIFO buffer...')
                        break
                    else:
                        fifo_data_df = fifo_data_df.append(
                            self._process_fifo_data_register(fifo_data_register),
                            ignore_index=True
                            )
                        fifo_data_register = self._request_fifo_data()
                
                if fifo_data_register:
                    # process and store
                    measurement_description = self._build_measurement_description_from_data(fifo_data_df, start_ts)
                    signals_df = self._build_signals_from_data(fifo_data_df)
                    self.current_measurement_id += 1 # count this new measure 
                    self._store_measurement_description_in_db(measurement_description)
                    self._store_waveform_in_db(signals_df, start_ts)
        else:
            logger.error('tdc dont have uptime')

def parse_arguments():
    parser = argparse.ArgumentParser(description='Create a fake host that can command a TDC.')
    parser.add_argument('-i', '--ip',type=str, help='IP of the TDC OSOTB server (instrument TCP server)')
    parser.add_argument('-p', '--port', type=str, help='Port of the TDC OSOTB server (instrument TCP server)')
    parser.add_argument('-id', '--ip-db',type=str, help='IP of the database server where the data is stored')
    parser.add_argument('-pd', '--port-db', type=str, help='port of the database server where the data is stored')
    parser.add_argument('-ud', '--username-db',type=str, help='Username to login to the database server where the data is stored')
    parser.add_argument('-psd', '--password-db', type=str, help='Password to login to the database server where the data is stored')
    parser.add_argument('-nd', '--name-db', type=str, help='Name of the database where the data is stored.')

    return parser.parse_args()


def cli():
    args = parse_arguments()
    
    fake_host = HeimdallFakeHost(   ip = args.ip, port = args.port,
                                    ip_db = args.ip_db, port_db = args.port_db,
                                    username_db = args.username_db, password_db = args.password_db,
                                    name_db = args.name_db )
    fake_host.measure()


if __name__ == "__main__":
    cli()