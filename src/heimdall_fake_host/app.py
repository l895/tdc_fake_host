"""
 *  TDC fake host app.
 *  This app mounts a web server to visualize and command the TDC.   
 *  
 *  Created on: Nov 17, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""
import time
import argparse
import logging
from heimdall_fake_host.fake_host import HeimdallFakeHost


logging.basicConfig(format='%(asctime)s %(message)s', datefmt='[%Y/%m/%d %I:%M:%S %p]', level=logging.INFO)
logger = logging.getLogger(__name__)

def parse_arguments():
    parser = argparse.ArgumentParser(description='create a fake host that can command a TDC.')
    parser.add_argument('-i', '--ip',type=str, help='IP of the TDC OSOTB server (instrument TCP server)')
    parser.add_argument('-p', '--port', type=str, help='Port of the TDC OSOTB server (instrument TCP server)')
    parser.add_argument('-id', '--ip-db',type=str, help='IP of the database server where the data is stored')
    parser.add_argument('-pd', '--port-db', type=str, help='port of the database server where the data is stored')
    parser.add_argument('-ud', '--username-db',type=str, help='Username to login to the database server where the data is stored')
    parser.add_argument('-psd', '--password-db', type=str, help='Password to login to the database server where the data is stored')
    parser.add_argument('-nd', '--name-db', type=str, help='Name of the database where the data is stored.')

    return parser.parse_args()


def run():
    args = parse_arguments()
    
    fake_host = HeimdallFakeHost(   ip = args.ip, port = args.port,
                                    ip_db = args.ip_db, port_db = args.port_db,
                                    username_db = args.username_db, password_db = args.password_db,
                                    name_db = args.name_db )

    # TODO: for now, its only measuring one time each 60 secs
    while True:
        fake_host.measure()
        time.sleep(60)

if __name__ == "__main__":
    run()