"""
 *  Fake TDC CLI. With this CLI we can mount an OSOTB server that mocks a
 *  TDC.
 *  Example: python3 -m heimdall_fake_tdc.fake_tdc -i 10.0.0.65 -p 8919 -c 16    
 *  
 *  Created on: Nov 4, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""

import random
import logging
import argparse
from datetime import datetime

from heimdall_common.osotb.server.server import OSOTBResource
from heimdall_common.osotb.server.tcpip import OSOTB_TCPServer

from .fake_tdc_data import generate_pulses_dataset, TDC_MAX_QUANTITY_OF_PULSES_PER_MEASUREMENT


logging.basicConfig(format='%(asctime)s %(message)s', datefmt='[%Y/%m/%d %I:%M:%S %p]', level=logging.INFO)
logger = logging.getLogger(__name__)


TDC_MAX_CHANNELS = 32 # max quantity of channels of the TDC


class HeimdallFakeTDC:
    def __init__(self, channels = 16, ip = "10.0.0.65", port = 8919):
        """
            Init the fake TDC. This will emulate the TDC, mounting an OSOTB server
            and sending the FIFO data when the fake host request it. In the FIFO data
            is the "fake" information of the TDC, that is, random pulses generated.
            @param channels the quantity of channels of the fake TDC.
            @param ip the IP of the server of the TDC.
            @param port the port of the OSOTB server of the TDC.
        """
        ## channels in the fake TDC
        self.channels = channels if channels <= TDC_MAX_CHANNELS else TDC_MAX_CHANNELS

        self.fake_fifo_buffer = [] ## list with the fake fifo data

        resources = [
            OSOTBResource(
                name = 'uptime',
                initial_value = 0,
                supported_methods = ['GET'],
                get_callback = self._update_uptime
            ),
            OSOTBResource(
                name = 'op_mode',
                initial_value = 'idle',
                supported_methods = ['GET', 'PUT'],
                put_callback = self._update_op_mode
            ),
            OSOTBResource(
                name = 'fifo_data',
                initial_value = '',
                supported_methods = ['GET'],
                get_callback = self._update_fifo_data
            ),
        ]

        self.osotb_server = OSOTB_TCPServer(ip = ip, port = port, resources = resources)

    def _update_uptime(self):
        """
            Update the value of the resource 'uptime'.
        """
        uptime = datetime.utcnow().isoformat()
        logger.debug('tdc is updating uptime... {}'.format(uptime))
        self.osotb_server.set_resource('uptime', uptime)

    def _update_fifo_data(self):
        """
            The real TDC stores the data in a FIFO buffer. We simulate this with 
            a big table and we update the value of the resource fifo data each time
            a GET to this resource is received.
        """
        logger.debug('tdc is updating fifo buffer data...')
        if self.fake_fifo_buffer:
            self.osotb_server.set_resource('fifo_data', self.fake_fifo_buffer.pop(0))
        else:
            self.osotb_server.set_resource('fifo_data', '0xFFFFFFFF')

    def _update_op_mode(self, payload):
        """
            The real TDC have two big operation modes:
            1) idle: stays doing nothing waiting for a measure request.
            2) measure: in this tate the TDC will be measuring its inputs.
            In this case, when we update the state from an idle state to a measure
            state we will refill the fake fifo buffer data.
        """
        logger.info('tdc is updating its operation mode to \'{}\' mode...'.format(payload))
        if payload == 'measure':
            logger.debug('filling fifo buffer with fake samples...')
            pulses_per_channel = dict()
            for i in range(0, self.channels):
                logger.info('filling fifo buffer of channel %d...', i)
                pulses_per_channel['{}'.format(i)] = random.randint(0, TDC_MAX_QUANTITY_OF_PULSES_PER_MEASUREMENT)     
            self.fake_fifo_buffer = generate_pulses_dataset(pulses_per_channel)
            self.osotb_server.set_resource('op_mode', 'idle') ## FIXME! for now only mock this and set to idle again.

    def work(self):
        """
            Set to work the OSOTB server of the TDC. The server will update the values of 
            the FIFO buffer when a GET is made to this resource. 
        """
        self.osotb_server.work()


def parse_arguments():
    parser = argparse.ArgumentParser(description='create a fake tdc that will mount an OSOTB server and can be commanded with an OSOTB client.')
    parser.add_argument('-i', '--ip',type=str, help='IP of the fake TDC')
    parser.add_argument('-p', '--port', type=int, help='port of the OSOTB server of the TDC')
    parser.add_argument('-c', '--channels', type=int, help='channels quantity of the fake TDC')
    return parser.parse_args()


def cli():
    args = parse_arguments()
    
    osotb_server = HeimdallFakeTDC(channels = args.channels, ip = args.ip, port = args.port)
    osotb_server.work()


if __name__ == "__main__":
    cli()