"""
 *
 *    Fake TDC data used by the fake tdc to emulate the measure
 *    of the high speed pulses.
 *    We have two types of data registers, the TDC outputs its data register in a bursts
 *    when the data transfer command is received. 
 *
 *    The two data registers are:
 *    1) The ones that have the pulses width inside, called "Width Data Register".
 *    2) The ones that have the temporal distance between two consecutive pulses, called "Pulse Distance Data Register"
 *
 *    Each data register have 32 bits. The type of the data register can be determined with the
 *    bit 19. 
 *
 *    1) Width Data Register:
 *    [31-27] : channel code, from channel b00000 = 0 to channel b11111 = 31
 *    [26-20] : start signal number, from start signal b0000000 = 0 to start signal b1111110 = 126.
 *        This identifies each pulse from the beginning.
 *    [19]    : 1 => Width Data Register
 *    [18-0]  : pulse width field, stop-start in picoseconds. From b000000000000000000 = 0ps to b1111111111111111111 = 524287ps
 *
 *    With this register we can know how many pulses was captured and they width 
 *    in a determined interval of time.
 *    We can measure up to 126 pulses of 524287ps in one measurement cycle.
 *    The max pulse-time that we can measure in one cycle is: 126*524287ps=66060162ps=66.06us
 *
 *    2) Pulse Distance Data Register:
 *    [31-27] : channel code, from channel b00000 = 0 to channel b11111 = 31
 *    [25-20] : previous start signal number, from start signal b0000000 = 0 to start signal b1111110 = 127.
 *        If 0, the data is the distance between the first pulse to the second pulse. 
 *        If 1, the data is the distance between the second pulse and the third pulse, and so on.
 *    [19]    : 0 => Pulse Distance Data register
 *    [18-0]  : distance from previous pulse, in picoseconds. From b000000000000000000 = 0ps to b1111111111111111111 = 524287ps
 *
 *    With this, we can know that if the pulses are distanced less than 524287ps each one.
 *    We cannot distinguish if the pulses are distanced more than this because we don't have more bits.
 *
 *    With this two types of registers, the max amount of time that we can measure in one
 *    measure cycle is:
 *    2 * 126 * 524287ps = 132120324 ps = 132.12 us 
 *
 *    Each channel follows this signal timeline:
 *
 *             __    __    __    __    __    __    __    __    __    __    __
 *    clock __|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |__|  |
 *
 *            x         x       x          x    x     x      x           x    
 *             ___       _       ____       __   __    ___    _____       __ 
 *    start __|   |_____| |_____|    |_____|  |_|  |__|   |__|     |_____|  |_ 
 *
 *                o       o          o        o    o      o        o        o
 *          __     _____   _____      _____    _    __     __       _____    _
 *    stop    |___|     |_|     |____|     |__| |__|  |___|  |_____|     |__| 
 *
 *    pulses 
 *    width   <--->     <->     <---->     <->  <->   <--->  <----->     <-> 
 *
 *    time
 *    between     <---->   <--->      <--->   <>   <->     <>       <--->
 *    pulses
 *
 *    Because the interface of the pulses is LVDS, the start and the stop signal are 
 *    the same signal negated.
 *    A rise slope in the start signal is taken as "start" and a rise slope in the 
 *    stop signal is taken as "stop". The difference between the this two slopes is 
 *    the width of the pulse.
 *
 * 3) End Data Register:
 *    Is a register that indicates that the FIFO buffer don't have more data. It's a word
 *  full of ones: 0xFFFFFFFF.
 *
 *  Created on: Nov 4, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""
import numpy as np

TDC_MAX_QUANTITY_OF_PULSES_PER_MEASUREMENT = 126 # The max quantity of pulses measured per cycle

def generate_pulses_dataset(pulses_per_channel):
    """
        Generate a array with fake output data registers from the TDC.
        @param pulses_per_channel is a dictionary with the quantity of pulses
        per channel that we want to emulate.
        @return a list of 32 bit data registers.
    """
    pulse_distance_data_register_bit = '1'
    width_data_register_bit = '0'
    data_registers = []
    for channel in pulses_per_channel:
        channel_field = '{0:05b}'.format(int(channel)) ## channel field is 5 bits long
        ## pulse width field is 19 bits long. We create the pulses with a normal distribution
        ## centered in 200000ps
        pulse_width_field = '{0:019b}'.format(int(abs(np.random.normal(loc=1, scale=0.3, size=1)[0])*200000)) 
        start_signal_number = '0000000'
        data_registers.append(
                int(
                    channel_field + start_signal_number + width_data_register_bit + pulse_width_field
                    ,2
                )
        )
        for p in range(1, pulses_per_channel[channel]):
            if p < TDC_MAX_QUANTITY_OF_PULSES_PER_MEASUREMENT:
                ## build the register with the width of the pulse
                start_signal_number = '{0:07b}'.format(p) ## start signal number is 7 bits width
                ## pulse width field is 19 bits long. We create the pulses with a normal distribution
                ## centered in 200000ps
                pulse_width_field = '{0:019b}'.format(int(abs(np.random.normal(loc=1, scale=0.3, size=1)[0])*200000)) 
                data_registers.append(
                        int(
                            channel_field +             ## channel field are bits [31-26]
                            start_signal_number +       ## start signal number field are bits [25-20]
                            width_data_register_bit +   ## pulse width data field is the bit 19
                            pulse_width_field           ## pulse width field are bits [18-0]
                            ,2
                        )
                )
                ## build the register with the distance from previous pulse
                previous_start_signal_number = '{0:07b}'.format(p-1) 
                ## distance from previous pulse field is 19 bits long. 
                ## We create the distances with normal distribution centered in 400000ps
                distance_from_previous_pulse_field = '{0:019b}'.format(int(abs(np.random.normal(loc=1, scale=0.3, size=1)[0])*400000))
                data_registers.append(
                        int(
                            channel_field +                     ## channel field are bits [31-26]
                            previous_start_signal_number +      ## previous start signal number field are bits [25-20]
                            pulse_distance_data_register_bit +  ## pulse distance data field is the bit 19
                            distance_from_previous_pulse_field  ## distance from previous pulse field are bits [18-0]
                            ,2
                        )
                )
            else:
                break
    return data_registers