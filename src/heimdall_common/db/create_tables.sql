--
-- PostgreSQL initial database where the data processed by
-- the TDC Fake Host will be stored.
--


-- The IDs are created as SERIAL to be auto-incremented each time
-- and in this manner we don't have to specify one if we want to insert data
-- into the tables

-- Create the table for the signals processed by the TDC Fake Host.
CREATE TABLE IF NOT EXISTS waveforms (
    id SERIAL PRIMARY KEY,
    measurement_id INT NOT NULL,
    start_time TIMESTAMP NOT NULL,
    channel INT NOT NULL,
    time_ps INT NOT NULL,
    signal_value INT NOT NULL
);

-- Create the table with the description of the measurements made by the TDC Fake Host.
CREATE TABLE IF NOT EXISTS measurements (
    id SERIAL PRIMARY KEY,
    start_time TIMESTAMP NOT NULL,
    channels TEXT NOT NULL,
    pulses_captured INT NOT NULL
);

-- Create the table for the logs of the TDC Fake Host.
CREATE TABLE tel_receiver_logs (
    id SERIAL PRIMARY KEY,
    date_time text NOT NULL,
    log_str text NOT NULL
);
