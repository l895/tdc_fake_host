from sqlalchemy import Column, Integer, Text, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class WaveformEntry(Base):
    """
        Model for the waveforms table.
        This table have the waveform of the signals measured with the TDC.
    """
    __tablename__ = 'waveform'
    id = Column(Integer, primary_key=True, autoincrement="auto")
    start_time = Column(DateTime, nullable=False)
    channel = Column(Integer, nullable=False)
    time_ps = Column(Integer, nullable=False)
    signal_value = Column(Integer, nullable=False)
    measurement_id = Column(Integer, nullable=False)

    def __init__(self, start_time, channel, time_ps, signal_value, measurement_id):
        self.start_time = start_time
        self.channel = channel
        self.time_ps = time_ps
        self.signal_value = signal_value
        self.measurement_id = measurement_id

    def serialize(self):
        """ Return the value of the table entry as a dictionary. """
        return {
            "id": self.id,
            "start_time": self.start_time,
            "channel": self.channel,
            "time_ps": self.time_ps,
            "signal_value": self.signal_value,
            "measurement_id": self.measurement_id
        }

    def __repr__(self):
        """ Return the value of the table entry as a string. """
        return f'waveform({self.start_time}, {self.channel}, {self.time_ps}, {self.signal_value}, {self.measurement_id})'

class MeasurementEntry(Base):
    """
        Model for the measurements table.
        This table registers the measurements made with the TDC.
        The fields are:
        * start_time: when was started the measurement. It's a datetime object.
        * channels: quantity of channels measured. It's an integer.
        * pulses_captured: quantity of pulses captured in the measurement.
    """
    __tablename__ = 'measurement'
    id = Column(Integer, primary_key=True, autoincrement="auto")
    start_time = Column(DateTime, nullable=False)
    channels = Column(Text, nullable=False)
    pulses_captured = Column(Integer, nullable=False)

    def __init__(self, start_time, channels, pulses_captured):
        self.start_time = start_time
        self.channels = channels
        self.pulses_captured = pulses_captured

    def serialize(self):
        """ Return the value of the table entry as a dictionary. """
        return {
            "id": self.id,
            "start_time": self.start_time,
            "channels": self.channels,
            "pulses_captured": self.pulses_captured
        }

    def __repr__(self):
        """ Return the value of the table entry as a string. """
        return f'measurement({self.id}, {self.start_time}, {self.channels}, {self.pulses_captured})'

class TDCFakeHostLogsEntry(Base):
    """
        Model for the table with the logs of the TDC Fake Host.
    """
    __tablename__ = 'tdc_fake_host_logs'
    id = Column(Integer, primary_key=True, autoincrement="auto")
    date_time = Column(Text, nullable=False)
    log_str = Column(Text, nullable=False)

    def __init__(self, date_time, log_str):
        self.date_time = date_time
        self.log_str = log_str

    def __repr__(self):
        return f'tdc_fake_host_logs({self.date_time}, {self.log_str})'
