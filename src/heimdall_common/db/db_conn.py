from sqlalchemy import create_engine, Column, Integer, String, Float, Text
from sqlalchemy.orm import sessionmaker
from .db_tables import Base
import logging
from time import monotonic


logger = logging.getLogger(__name__)

class HeimdallDB:
    def __init__(self, username, password, ip, port, db_name):
        """
            Creates a Heimdall database object.
            @param username the username to login to the database.
            @param password the password to login to the database.
            @param ip the IP of the server that stores the database.
            @param port hte port of the server that stores the database.
            @param db_name the name of the database where the tables are stored.
        """
        self.conn_path = "postgresql://{}:{}@{}:{}/{}".format(username, password, ip, port, db_name)
        self.engine = create_engine(self.conn_path)
        Session = sessionmaker(bind=self.engine) ## factory function binded to engine
        self.session = Session()
    
        init_time_s = monotonic()        
        while (monotonic() - init_time_s < 60):
            try:
                Base.metadata.create_all(self.engine)
                break
            except Exception as e:
                logger.error("Error creating tables or initializating table models! ({})".format(e))


    def add_entry(self, entry):
        """
            Add an entry to a table from the TDC database.
        """
        try:
            self.session.add(entry)
            self.session.commit()
        except Exception as e:
            logger.error("Error adding entry to a table of the database! ({})".format(e))