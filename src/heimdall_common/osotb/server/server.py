"""
 *  OSOTB server base class. Can be used to 
 *  implement different OSOTB servers using different interfaces.
 *  
 *  Created on: Nov 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""

from abc import ABC, abstractmethod
import json
import logging
from ..common import OSOTB_BASE, OSOTBError

logger = logging.getLogger(__name__)

class OSOTBResource:
    """
        Class used to implement resources.
        The resources have names, and supported methods.
        @param name is the identification of the resource, for example, "uptime" can 
        represent the time that the server has been up.
        @param value is the value of the resource, for the resouce "uptime", can be 100 hours.
        @param supported methods are a list of methods that can be handled by the resource.
    """
    def __init__(self, initial_value, name, supported_methods, get_callback = None, put_callback = None):
        self._supported_methods = supported_methods
        self._name = name
        self._value = initial_value
        self._get_callback = get_callback
        self._put_callback = put_callback

    @property
    def name(self):
        return self._name
    
    @property
    def value(self):
        return self._value
    
    @value.setter
    def value(self, val):
        self._value = val

    @property
    def methods(self):
        return self._supported_methods

class OSOTBServer(ABC):
    """
        This is a base class used to make other classes that setup an OSOTB server.
        This base class have a method called 'process_request' that process
        an OSOTB packet and generates the response. Also, have methods to update
        the resource and methods table of the server.
        Also have an abstract method called "send_response" that a child class
        can implement through a desired interface (TCP/IP, UART, etc).
    """
    @abstractmethod
    def __init__(self, resources:[OSOTBResource]):
        """
            This constructor serves as a guidance for the child classes.
            @param resources a list of the resources of the server with the supported methods.
            The type of this resources must be OSOTBResource
        """
        self._resources = dict()
        for res in resources:
            self._resources[res.name] = res
        
        logger.debug('resources of the server: {}'.format(self._resources))

    def get_resource(self, resource_name):
        """
            Get the value of a resource of the server by its name.
            @param resource_name the name of the resource.
            @return the value of the resource. If the resources don't exists then return None.
            Note: the value of the resource could be None too. 
        """
        value = None
        if resource_name in self._resources.keys():
            value = self._resources[resource_name].value
        return value

    def set_resource(self, resource_name, set_value):
        """
            set the value of a resource of the server.
            @param resource_name the name of the resource.
            @param value the value to be set on that resource.
        """
        if resource_name in self._resources.keys():
            logger.debug("set {} to {}".format(resource_name, set_value))
            self._resources[resource_name].value = set_value
        
    def create_response(self, status, resource, payload = None):
        """
            Create an OSOTB response.
            @param status the status of the response, see OSOTB_COMMON.responses dictionary.
            @param resource the resource of the request.
            @param payload the payload in the response. Usually is only used with the responses
            to GET requests.
        """
        osotb_response_dict = {
                "header":
                {
                    "ver": OSOTB_BASE['proto_version'],
                    "method": 'RESPONSE',
                    "status" : status,
                    "resource": resource
                },
                "payload": payload
            }
        return json.dumps(osotb_response_dict)+"\n"

    def parse_incoming_packet(self, received_pkt):
        """
            Parse a series of bytes to see if there is an osotb request and
            returns a json with that request.
            @param received_pkt the string received.
            @return a json with the request.
        """
        parsed_json = json.loads(received_pkt)
        if (('header' in parsed_json.keys()) and ('payload' in parsed_json.keys())):
            if  (('ver' in parsed_json['header'].keys()) and\
                 ('method' in parsed_json['header'].keys()) and\
                 ('resource' in parsed_json['header'].keys())):
                logger.debug("the received packet have a good format!: {}".format(received_pkt))
            else:
                logger.warning("the received packet doesn't have the correct format, no version, method and/or resource field")
                parsed_json = None
        else:
            logger.warning("the received packet doesn't have the correct format, no header and/or no payload")
            parsed_json = None

        return parsed_json

    def process_request(self, resource, method, payload):
        """
            Process a request to this resource. First check if the method
            of the request is support by the resource. If it is supported
            then set (PUT) or return the value (GET) of the resource.
            @param resource the resource of the request.
            @param method the method of the request.
            @param payload the payload of the request
            @return the response that we must to send to the client.
        """
        logger.debug('processing request... resource = {}, method = {}, payload = {}'.format(resource, method, payload))
        ret = OSOTBError.OK
        response_payload = None # is the payload of the osotb response packet
        if method in OSOTB_BASE['supported_methods']:
            if resource in self._resources.keys():
                if method in self._resources[resource].methods:

                    if method == OSOTB_BASE['get_string']:
                        if self._resources[resource]._get_callback:
                            self._resources[resource]._get_callback()
                        else:
                            logger.debug('no get callback for resource {}'.format(resource))
                        response_payload = self._resources[resource].value ## get resource value
                    elif method == OSOTB_BASE['put_string']:
                        self._resources[resource].value = payload ## set resource value
                        if self._resources[resource]._put_callback:
                            self._resources[resource]._put_callback(payload)
                        else:
                            logger.debug('no put callback for resource {}'.format(resource))
                    else:
                        logger.error('error processing request!')
                        ret = OSOTBError.INVALID_PACKET_FORMAT

                else:
                    logger.warning('the resource don\'t support the method ({}) of the request!'.format(method))
                    ret = OSOTBError.UNKNOWN_RESOURCE
        else:
            logger.error('method is not supported by the protocol {}'.format(method))
            ret = OSOTBError.METHOD_NOT_SUPPORTED

        if ret == OSOTBError.OK:
            response_packet = self.create_response(OSOTB_BASE['responses']['200'], resource, response_payload)
        else:
            response_packet = self.create_response(OSOTB_BASE['responses']['404'], resource, response_payload)

        return response_packet