"""
 *  OSOTB TCP server CLI. With this CLI we can mount an OSOTB server.
 *  
 *  Created on: Nov 13, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""

import socket
import argparse
import logging
import yaml

from .server import OSOTBServer, OSOTBResource
from ..common import OSOTB_BASE

logger = logging.getLogger(__name__)

class OSOTB_TCPServer(OSOTBServer):
    """
        A class that can mount an OSOTB server through a TCP/IP socket.
    """
    def __init__(self, ip, port, resources:[OSOTBResource]):
        self.ip = ip
        self.port = port
        self._resources = dict()
        for res in resources:
            self._resources[res.name] = res
        
        logger.debug("creating OSOTB Server object with ip:port {}:{} and resources {}".format(
                self.ip, self.port, 
                {str(res_name) for res_name in self._resources.keys()}
                )
            )
        
    def work(self):
        """
            Blocking call where the server will be up waiting for requests of 
            a client.
        """
        while True:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                # enable to reause address, if the client disconnects
                # we can set-up again the server up with the same port
                s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                s.bind((self.ip, self.port))
                logger.debug('listening on {}:{}'.format(self.ip, self.port))
                s.listen()
                conn, addr = s.accept()
                with conn:
                    logger.debug('The client {} was connected'.format(addr))
                    while True:
                        data = conn.recv(1500)  ## TODO: this is fixed to 1500 bytes, must be refined!
                        if not data:    ## if we receive an empty string of bytes then client closed the connection
#                            logger.warning('connection ended by client!')
                            break
                        else:
                            logger.debug('parsing request from data...')
                            request = self.parse_incoming_packet(data)
                            if request:
                                response = self.process_request(    method=request['header']['method'],
                                                                    resource=request['header']['resource'],
                                                                    payload=request['payload'])
                            else:
                                response = self.create_response(OSOTB_BASE['responses']['404'], "", "bad format")

                            logger.debug('sending response to client... {}'.format(response))
                            conn.sendall(bytes(response, encoding='utf-8'))

def parse_arguments():
    parser = argparse.ArgumentParser(description='mount an OSOTB server with a configuration file.')
    parser.add_argument('-c', '--cfg-file',type=str, help='the relative path to the configuration file of the server')
    return parser.parse_args()

def cli():
    args = parse_arguments()
    
    with open(args.cfg_file, 'r') as stream:
        try:
            sv_cfg = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logger.error(exc)

    resources = []
    for res_name in sv_cfg['resources']:
        resources.append(
            OSOTBResource(
                initial_value = sv_cfg['resources'][res_name]['initial_value'],
                name = res_name,
                supported_methods = sv_cfg['resources'][res_name]['methods']
            )
        )

    osotb_server = OSOTB_TCPServer(ip = sv_cfg['ip'], port = sv_cfg['port'], resources = resources)
    osotb_server.work()


if __name__ == "__main__":
    cli()
