"""
 *  OSOTB TCP client CLI. With this CLI we can make OSOTB request to a server.
 *  Example: python3 osotb_client.py -r UPTIME -m GET -i 192.168.0.83 -p 49253    
 *  
 *  Created on: Jul 11, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
"""
from json.decoder import JSONDecodeError
import socket
import argparse
import logging
import time
import json
from .client import OSOTBClient

logger = logging.getLogger(__name__)

class OSOTB_TCPClient(OSOTBClient):
    """
        A class that send OSOTB request through a TCP/IP socket.
    """
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send_request(self, osotb_packet, response_timeout_s=15):
        """
            Send an OSOTB request to a server.
            @param osotb_packet must be the json of the OSOTB request.
            @param ip the IP of the server.
            @param port the port of the server
            @return the response or None if an error ocurred.
        """
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to the port where the server is listening
        retries = 3
        while retries > 0:
            try:
                logger.debug("connecting to : {}:{}".format(self.ip, int(self.port)))
                sock.connect((self.ip, int(self.port)))
                break
            except Exception as exc:
                logger.error('error connecting to server {}'.format(exc))
                retries -=0
                time.sleep(2)
                return None

        response = b""
        try:
            # Send request
            logger.debug("sending {}".format(osotb_packet.encode()))
            sock.sendall(osotb_packet.encode())

            # Look for the response. The response packets must end with a '\n'. Also, the wait time
            # will end if the maximum time is reached.
            init_wait_time_s = time.monotonic()
            while True:
                data = sock.recv(1500)
                if b'\n' in data:
                    response = response + data.partition(b'\n')[0] # cut the content after the '\n' and the '\n' too 
                    logger.debug("received {}".format(response))
                    break
                elif (init_wait_time_s - time.monotonic()) > response_timeout_s:
                    logger.warning("timeout! (time > {})".format(response_timeout_s))
                else:
                    response = response + data
        except Exception as ex:
            logger.error("Error ocurred! (exception={}, res={})".format(ex, response))
        finally:
            logger.debug("closing socket")
            sock.close()

        parsed_response = None
        if len(response) > 0:
            try:
                parsed_response = json.loads(response)
            except Exception as exc:
                logger.error('error deserializing osotb response! (exception={}, res={})'.format(exc, response))

        return parsed_response


def parse_arguments():
    parser = argparse.ArgumentParser(description='Send OSOTB request to a server.')
    parser.add_argument('-r', '--resource',type=str, help='the resource to make a request')
    parser.add_argument('-md', '--method',type=str, help='method used on the resource')
    parser.add_argument('-i', '--ip',type=str, help='IP of the server to make the request')
    parser.add_argument('-p', '--port', type=str, help='port of the server to make the request')
    parser.add_argument('-pay', '--payload', type=str, help='payload of the request')
    return parser.parse_args()

def cli():
    args = parse_arguments()
    if (args.resource is None) or (args.method is None) or (args.ip is None) or (args.port is None):
        logger.error("missing arguments (resource={}, method={}, ip={}, port={}).".format(args.resource, args.method, args.ip, args.port))

    osotb_tcpip = OSOTB_TCPClient(ip = args.ip, port = args.port)
    osotb_packet = osotb_tcpip.create_osotb_req_packet(resource=args.resource, method=args.method, payload=args.payload)
    osotb_tcpip.send_request(osotb_packet)


if __name__ == "__main__":
    cli()
