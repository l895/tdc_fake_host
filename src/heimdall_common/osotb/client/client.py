from abc import ABC, abstractmethod
import json
import logging

from ..common import OSOTB_BASE, OSOTBError

logger = logging.getLogger(__name__)

class OSOTBClient(ABC):
    """
        This is a base class used to make other classes that can send OSOTB request.
        This base class have a method called 'create_osotb_req_packet' that
        creates a command in a string form and then have an bastract method that
        sends the packet through any interface. The idea is that the child class
        implement this 'send_request' method in a choosen interface (TCP/IP, UART, etc).
    """
    def create_osotb_req_packet(self, resource, method, payload=""):
        """
            Create an OSOTB request packet. This packet is the payload of a TCP packet.
            @param resource the resource that is targetting the request.
            @param method the method with we are targetting the resource.
            @param payload (optional) the payload that is included in the request. 
        """
        osotb_packet_dict = {
                "header":
                {
                    "ver": OSOTB_BASE['proto_version'],
                    "method": method,
                    "resource": resource
                },
                "payload": payload
            }
        return json.dumps(osotb_packet_dict)+"\n"

    def parse_response(self, received_pkt):
        """
            Parse a series of bytes to see if there is an osotb response and
            returns a json with the response.
            @param received_pkt the string received.
            @return a json with the response.
        """
        parsed_json = json.loads(received_pkt)
        try:
            ## Check the parsed json have the correct fields
            method = parsed_json['header']['method']
            resource = parsed_json['header']['resource']
            status = parsed_json['header']['status']
        except KeyError as err:
            parsed_json = None
            logger.warning("the received packet doesn't have the correct format, the following field is not correctly formatted: {}".format(err))
        return parsed_json

    @abstractmethod
    def send_request(self, response_timeout_s):
        pass
