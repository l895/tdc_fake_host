from enum import Enum

OSOTB_BASE = {
    'proto_version' : "100",
    'get_string' : "GET",
    'put_string' : "PUT",
    'supported_methods' : [ "GET", "PUT" ],
    'responses' : 
    {
        "200" : "200 Ok",
        "404" : "404 Not Found",
        "500" : "500 Internal Server Error"
    }
}

class OSOTBError(Enum):
    OK = 0
    ERROR = 1
    INTERNAL_SERVER_ERROR = 2
    METHOD_NOT_SUPPORTED = 3
    UNKNOWN_RESOURCE = 4
    INVALID_PACKET_FORMAT = 5