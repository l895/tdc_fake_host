from unittest import TestCase
from pandas.testing import assert_frame_equal
import pandas as pd
from datetime import datetime

from heimdall_fake_host.fake_host import HeimdallFakeHost


def test_process_fifo_data_register():
    """ Test the method that parses the data fifo registers. """    
    fake_host = HeimdallFakeHost()
    fifo_data_register = int(   '00010' +    # channel 2
                                '1111000' +  # pulse number 120 
                                '1' +        # pulse width register
                                '1000001000011111010', # pulse width 266490
                                2
                            )
    expected_output = {
        'channel' : 2,
        'pulse_number' : 120,
        'register_type' : 'pulse_width',
        'time_width_ps' : 266490
    }
    process_output = fake_host._process_fifo_data_register(fifo_data_register)
    TestCase().assertDictEqual(expected_output, process_output)

    fifo_data_register = int(   '00100' +    # channel 4
                                '1111000' +  # pulse number 120 
                                '0' +        # pulse distance width register
                                '1010010000001110010', # distance from previous pulse 335986
                                2
                            )
    expected_output = {
        'channel' : 4,
        'pulse_number' : 120,
        'register_type' : 'pulse_distance_width',
        'time_width_ps' : 335986
    }
    process_output = fake_host._process_fifo_data_register(fifo_data_register)
    TestCase().assertDictEqual(expected_output, process_output)

def test_build_signals_from_data():
    """ Test the method that builds the waveforms from data fifo registers. """
    fifo_data_df = pd.DataFrame(columns = ['channel', 'pulse_number', 'time_width_ps', 'register_type'])
    fake_data_registers = [
        {'channel' : 0, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},

        {'channel' : 1, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},

        {'channel' : 3, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},
    ]
    fifo_data_df = fifo_data_df.append(fake_data_registers)

    expected_signals_df = pd.DataFrame(columns = ['channel', 'time_ps', 'value'])
    fake_signals_data = [
        {'channel' : 0, "time_ps" : 0, "value" : 1},    #   0
        {'channel' : 0, "time_ps" : 100, "value" : 1},  # (  0 + 100)
        {'channel' : 0, "time_ps" : 101, "value" : 0},  # (  0 + 100) + 1
        {'channel' : 0, "time_ps" : 399, "value" : 0},  # (  0 + 100) + 300 - 1
        {'channel' : 0, "time_ps" : 400, "value" : 1},  # (  0 + 100) + 300
        {'channel' : 0, "time_ps" : 490, "value" : 1},  # (100 + 300) + 90
        {'channel' : 0, "time_ps" : 491, "value" : 0},  # (100 + 300) + 90  + 1 
        {'channel' : 0, "time_ps" : 649, "value" : 0},  # (100 + 300) + 90 + 160 -1
        {'channel' : 0, "time_ps" : 650, "value" : 1},  # ...
        {'channel' : 0, "time_ps" : 740, "value" : 1},
        {'channel' : 0, "time_ps" : 741, "value" : 0},
        {'channel' : 0, "time_ps" : 849, "value" : 0},
        {'channel' : 0, "time_ps" : 850, "value" : 1},
        {'channel' : 0, "time_ps" : 910, "value" : 1},
        {'channel' : 0, "time_ps" : 911, "value" : 0},
        {'channel' : 0, "time_ps" : 1109, "value" : 0},
        {'channel' : 0, "time_ps" : 1110, "value" : 1},
        {'channel' : 0, "time_ps" : 1140, "value" : 1},
        {'channel' : 0, "time_ps" : 1141, "value" : 0},
        {'channel' : 0, "time_ps" : 1209, "value" : 0},
        {'channel' : 0, "time_ps" : 1210, "value" : 1},
        {'channel' : 0, "time_ps" : 1250, "value" : 1},
        {'channel' : 0, "time_ps" : 1251, "value" : 0},

        {'channel' : 1, "time_ps" : 0, "value" : 1},    #   0
        {'channel' : 1, "time_ps" : 100, "value" : 1},  # (  0 + 100)
        {'channel' : 1, "time_ps" : 101, "value" : 0},  # (  0 + 100) + 1
        {'channel' : 1, "time_ps" : 399, "value" : 0},  # (  0 + 100) + 300 - 1
        {'channel' : 1, "time_ps" : 400, "value" : 1},  # (  0 + 100) + 300
        {'channel' : 1, "time_ps" : 490, "value" : 1},  # (100 + 300) + 90
        {'channel' : 1, "time_ps" : 491, "value" : 0},  # (100 + 300) + 90  + 1 
        {'channel' : 1, "time_ps" : 649, "value" : 0},  # (100 + 300) + 90 + 160 -1
        {'channel' : 1, "time_ps" : 650, "value" : 1},  # ...
        {'channel' : 1, "time_ps" : 740, "value" : 1},
        {'channel' : 1, "time_ps" : 741, "value" : 0},
        {'channel' : 1, "time_ps" : 849, "value" : 0},
        {'channel' : 1, "time_ps" : 850, "value" : 1},
        {'channel' : 1, "time_ps" : 910, "value" : 1},
        {'channel' : 1, "time_ps" : 911, "value" : 0},
        {'channel' : 1, "time_ps" : 1109, "value" : 0},
        {'channel' : 1, "time_ps" : 1110, "value" : 1},
        {'channel' : 1, "time_ps" : 1140, "value" : 1},
        {'channel' : 1, "time_ps" : 1141, "value" : 0},
        {'channel' : 1, "time_ps" : 1209, "value" : 0},
        {'channel' : 1, "time_ps" : 1210, "value" : 1},
        {'channel' : 1, "time_ps" : 1250, "value" : 1},
        {'channel' : 1, "time_ps" : 1251, "value" : 0},

        {'channel' : 3, "time_ps" : 0, "value" : 1},    #   0
        {'channel' : 3, "time_ps" : 100, "value" : 1},  # (  0 + 100)
        {'channel' : 3, "time_ps" : 101, "value" : 0},  # (  0 + 100) + 1
        {'channel' : 3, "time_ps" : 399, "value" : 0},  # (  0 + 100) + 300 - 1
        {'channel' : 3, "time_ps" : 400, "value" : 1},  # (  0 + 100) + 300
        {'channel' : 3, "time_ps" : 490, "value" : 1},  # (100 + 300) + 90
        {'channel' : 3, "time_ps" : 491, "value" : 0},  # (100 + 300) + 90  + 1 
        {'channel' : 3, "time_ps" : 649, "value" : 0},  # (100 + 300) + 90 + 160 -1
        {'channel' : 3, "time_ps" : 650, "value" : 1},  # ...
        {'channel' : 3, "time_ps" : 740, "value" : 1},
        {'channel' : 3, "time_ps" : 741, "value" : 0},
        {'channel' : 3, "time_ps" : 849, "value" : 0},
        {'channel' : 3, "time_ps" : 850, "value" : 1},
        {'channel' : 3, "time_ps" : 910, "value" : 1},
        {'channel' : 3, "time_ps" : 911, "value" : 0},
        {'channel' : 3, "time_ps" : 1109, "value" : 0},
        {'channel' : 3, "time_ps" : 1110, "value" : 1},
        {'channel' : 3, "time_ps" : 1140, "value" : 1},
        {'channel' : 3, "time_ps" : 1141, "value" : 0},
        {'channel' : 3, "time_ps" : 1209, "value" : 0},
        {'channel' : 3, "time_ps" : 1210, "value" : 1},
        {'channel' : 3, "time_ps" : 1250, "value" : 1},
        {'channel' : 3, "time_ps" : 1251, "value" : 0},        
    ]
    expected_signals_df = expected_signals_df.append(fake_signals_data)

    fake_host = HeimdallFakeHost()
    signals_df = fake_host._build_signals_from_data(fifo_data_df)

    assert_frame_equal(expected_signals_df, signals_df)


def test_build_measurement_description_from_data():
    """ Test the method that builds the measurements descriptions from data fifo registers. """
    fifo_data_df = pd.DataFrame(columns = ['channel', 'pulse_number', 'time_width_ps', 'register_type'])
    fake_data_registers = [
        {'channel' : 0, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 0, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 0, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},

        {'channel' : 1, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 1, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 1, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},

        {'channel' : 3, 'pulse_number' : 0, "time_width_ps" : 100, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 1, "time_width_ps" : 300, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 1, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 2, "time_width_ps" : 160, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 2, "time_width_ps" : 90, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 3, "time_width_ps" : 110, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 3, "time_width_ps" : 60, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 4, "time_width_ps" : 200, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 4, "time_width_ps" : 30, "register_type" : 'pulse_width'},
        {'channel' : 3, 'pulse_number' : 5, "time_width_ps" : 70, "register_type" : 'pulse_distance_width'},
        {'channel' : 3, 'pulse_number' : 5, "time_width_ps" : 40, "register_type" : 'pulse_width'},
    ]
    fifo_data_df = fifo_data_df.append(fake_data_registers)
    start_ts = datetime(2021, 6, 10)
    expected_measurement_description = {
        'start_time': start_ts,
        'channels': '{0, 1, 3}',
        'pulses_captured': 6*3
    }
    fake_host = HeimdallFakeHost()
    measurement_description = fake_host._build_measurement_description_from_data(fifo_data_df, start_ts)
    TestCase().assertDictEqual(expected_measurement_description, measurement_description)