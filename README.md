# tdc_fake_host

The Heimdall TDC developed by LabOSat team is a device that can sample high speed digital pulses and analyze them. The outputs of the TDC are the arrival times and the width of the digital pulses. This device has two interfaces with a host:
* a serial port
* an Ethernet port

The host can command the TDC through this interfaces talking with a determined protocol.
The aim of the TDC fake host is to have an interface to command the TDC and analyze the data reported by it. 

![TDC system designed by LabOSat](img/1_tdc_block_design.png "LabOSat TDC system")

## Fake Host
The fake host can be used as a CLI (Command Line Interface) to command the TDC. It can:
* Start a measure
* Download the report
* Store the reports in a database
* Plot the data in a dashboard

### Fake Host workflow

The fake host run 3 services:
* heimdall_fake_host: this service can command a TDC through ethernet or serial port. Cand send commands, download the data and store it in a database.
* heimdall_database: this is the database where the data of the TDC is stored. It's a postgres database.
* heimdall_dashboard: this is a Dash web app (based in Flask) where the data information of the reports are plotted and information of the TDC can be seen.

![Fake host workflow](img/3_fake_host_workflow.png "Fake host workflow")

There is a custom network called `heimdall_net` inside the docker-engine where all this services have static IPs.
The IPs and ports of the containers can be configured in the `.env` in the root dir.

### Develop & Test environment

In this repository we have a dockerized environment to test and develop the fake host.

We have a dockerized service called `heimdall_fake_tdc` that emulates the TDC and generates the reports when is commanded by a host.
Then, the python runner `heimdall_fake_host` can connect to the TDC emulator, start a measurement and download the report. This runner will store the reports information in the postgres database `heimdall_database` and then the Dash server `heimdall_dashboard` will plot it.

![Fake host test environment](img/4_fake_host_test_env.png "Fake host test env")

### Unit-tests
The methods of this project are unit-tested with pytest. The unit-tests are in the directory tests and can be launched with pytest:

`python3 -m pytest tests`

or with make:

`make tests`


## Heimdall Database

We can explore the database manually. The steps are the following:
1. enter to the postgres container with `docker exec -it <CONTAINER_HASH> /bin/bash`
2. login to the database with `psql -U postgres` (postgres is the username)
3. See the current databases with the command `\l`:

```shell
postgres-# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 heimdall  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 postgres  | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
 template0 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.utf8 | en_US.utf8 | =c/postgres          +
           |          |          |            |            | postgres=CTc/postgres
(4 rows)
```

4. Connect to the heimdall database with `\c heimdall`:

```shell
postgres-# \c heimdall
You are now connected to database "heimdall" as user "postgres".
```

5. List tables in the heimdall database with `\d`:

```shell
heimdall-# \d
                    List of relations
 Schema |           Name            |   Type   |  Owner   
--------+---------------------------+----------+----------
 public | tdc_fake_host_logs        | table    | postgres
 public | tdc_fake_host_logs_id_seq | sequence | postgres
 public | waveform                  | table    | postgres
 public | waveform_id_seq           | sequence | postgres
(4 rows)
```

6. Then we can see if the waveform table have some values with a classic sql sentence:

```shell
heimdall=# select * from waveform limit 10;
 id |        start_time         | channel | time_ps | signal_value 
----+---------------------------+---------+---------+--------------
  1 | 2021-11-19 22:30:18.77461 |       0 |       0 |            1
  2 | 2021-11-19 22:30:18.77461 |       0 |   24653 |            1
  3 | 2021-11-19 22:30:18.77461 |       0 |   24654 |            0
  4 | 2021-11-19 22:30:18.77461 |       0 |   24654 |            0
  5 | 2021-11-19 22:30:18.77461 |       0 |   24655 |            1
  6 | 2021-11-19 22:30:18.77461 |       0 |   68662 |            1
  7 | 2021-11-19 22:30:18.77461 |       0 |   68663 |            0
  8 | 2021-11-19 22:30:18.77461 |       0 |   68663 |            0
  9 | 2021-11-19 22:30:18.77461 |       0 |   68664 |            1
 10 | 2021-11-19 22:30:18.77461 |       0 |  116065 |            1
(10 rows)
```

## Generating fake waveforms

We can generate fake waveforms with this:

```python
# first generating fake data registers from the tdc:
from heimdall_fake_tdc.fake_tdc_data import generate_pulses_dataset
pulses_per_channel = { 
'0':200, 
'1':98, 
'2':120, 
'3':3, 
'4':69, 
'5':191, 
'6':222, 
'7':100, 
'8':17, 
'9':98, 
'10':120, 
'11':0, 
'12':56, 
'13':78, 
'14':198, 
'15':143 
}
data_registers = generate_pulses_dataset(pulses_per_channel)

# then processing the registers and generating the waveforms
import pandas as pd
from heimdall_fake_host.fake_host import HeimdallFakeHost
fake_host = HeimdallFakeHost() ## if there are retries this can take 60 secs
fifo_data_df = pd.DataFrame() 
for register in data_registers: 
    fifo_data_df = fifo_data_df.append( 
        fake_host._process_fifo_data_register(register), 
        ignore_index=True)
signal_df = fake_host._build_signals_from_data(fifo_data_df)
```

